import autograd.numpy as np
import scipy.constants as cons
import multilayer
multilayer.set_backend('autograd') 

ev2omega = lambda x:x*cons.e/cons.hbar
omega2ev = lambda x:cons.hbar*x/cons.e

def DL_epsilon(w,wa,w0,gamma):
    psum = wa**2/(w**2-w0**2+1j*w*gamma)
    return 1-np.sum(psum)

def bulk_fun(wa,w0,omega_integrate,kbeta_integrate,gamma=0.004,distance=10e-9,T0=300,mode='p'):
    epsbkg = lambda w:DL_epsilon(omega2ev(w),wa,w0,gamma)

    obj = multilayer.obj(distance,epsbkg,0,0)
    obj.get_epsilon([1,1])
    y,htc = obj.htc_mode(T0,omega_integrate,kbeta_integrate,mode)
    return y/1e5,htc


def DL_epsilon_list(w,Nlayer,wa_list,w0,gamma):
    N = len(w0)
    y = []
    
    for i in range(Nlayer):
        wa = wa_list[i*N:(i+1)*N]
        ep = DL_epsilon(w,wa,w0,gamma)
        y.append(ep)
        
    return np.array(y)

def mul_fun(wa_list,w0,Nlayer,dx,omega_integrate,kbeta_integrate,epsbkg=1.,gamma=0.004,distance=10e-9,T0=300,mode='p'):
    
    ep1 = lambda w:DL_epsilon_list(omega2ev(w),Nlayer,wa_list,w0,gamma)
    ep2 = ep1
    
    obj = multilayer.obj(distance,epsbkg,dx,Nlayer)
    obj.get_epsilon([ep1,ep2])
    y,htc = obj.htc_mode(T0,omega_integrate,kbeta_integrate,mode)
    return y/1e5,htc
