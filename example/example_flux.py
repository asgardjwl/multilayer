import numpy as np
import multilayer

#  consider a geometry like this:
#  epsback| 1  |  2 | ... |  N || distance ||  N  |  ... |  2  |  1  | epsbkg

# each layer has the same thickness dx
dx = 10e-9  # 10 nm
# The separation between two multilayers
distance = 100e-9  # 100 nm
# the number of layers in each multilayer
N = 2
# the background epsilon away from the multilayer
epsbkg = 1

# ------  setup the multilayer system  ------ #
obj = multilayer.obj(distance,epsbkg,dx,N)


# what's the epsilon? the numbering is shown above
ep1 = np.array([-2+1j, -4+0.1j])
ep2 = np.array([10+10j, -5+2j])
epsilon = [ep1,ep2]
obj.get_epsilon(epsilon)

# compute heat transfer at a particular k_beta (in unit of k0, k0 = omega/c = 2pi/lam)
lam = 1e-6  # wavelength
k0 = 2*np.pi/lam

kbeta = 2.0
mode = 'p' # p/s polarization
flux = obj.flux_omega_k_mode(kbeta,k0,mode)
print(flux)

# compute heat transfer integrated over certain k_beta range, e.g. [2,10]k0, approximate the integral with kN even sampling points
kN = 100
kbeta_integrate = [2,10,kN]
flux = obj.flux_omega(kbeta_integrate,k0,mode)
print(flux)
