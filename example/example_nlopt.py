""" Topology optimization of Re[epsilon]."""
""" Nlopt is needed """

import multilayer
multilayer.set_backend('autograd')  # important!!

import numpy as npf
import autograd.numpy as np
from autograd import grad

try:
    import nlopt
    NL_AVAILABLE = True
except ImportError:
    NL_AVAILABLE = False

if NL_AVAILABLE == False:
    raise Exception('Please install NLOPT')

# each layer has the same thickness dx
dx = 10e-9  # 10 nm
# The separation between two multilayers
distance = 100e-9  # 100 nm
# the number of layers in each multilayer
N = 10
# the background epsilon away from the multilayer
epsbkg = 1

# ------  setup the multilayer system  ------ #
obj = multilayer.obj(distance,epsbkg,dx,N)

# let's fixed the material loss, but optimize Re[epsilon]
epsimag1 = np.random.random(N)*1j
epsimag2 = np.random.random(N)*1j

lam = 1e-6  # wavelength
k0 = 2*np.pi/lam
mode = 'p' # p/s polarization
kN = 100
kbeta_integrate = [1+1e-5,10,kN]

def fun(x):
    '''
    x = np.array([Re[ep1],Re[ep2]])
    '''
    epsilon = [x[:N]+epsimag1,x[N:]+epsimag2]
    obj.get_epsilon(epsilon)
    flux = obj.flux_omega(kbeta_integrate,k0,mode)

    return flux


# ----  nlopt function  ---- #
ctrl = 0
grad_fun = grad(fun)
def fun_nlopt(x,gradn):
    global ctrl
    gradn[:] = grad_fun(x)
    y = fun(x)
    
    print('Step = ',ctrl,', flux = ',y)
    ctrl += 1
    return fun(x)

# set up NLOPT
ndof = N*2
init = np.random.random(ndof)
# let Re[ep] vary between [-5,5]
lb=np.ones(ndof,dtype=float)*-5.
ub=np.ones(ndof,dtype=float)*5.

opt = nlopt.opt(nlopt.LD_MMA, ndof)
opt.set_lower_bounds(lb)
opt.set_upper_bounds(ub)

opt.set_ftol_rel(1e-10)
opt.set_maxeval(100)

opt.set_max_objective(fun_nlopt)
x = opt.optimize(init)
