import nlopt
import autograd.numpy as np
from autograd import grad
from util import ev2omega,omega2ev,DL_epsilon,mul_fun
# in Owen's paper, sp, wo is linspace(0,0.4eV,100), gamma=0.004eV

distance = 10e-9
T0 = 300

gamma = 0.004
No = 50
w0 = np.linspace(0,0.1,No)

kbeta_integrate = [0,2000,100]
omega_integrate = [0.1e14,5e14,100]

Nlayer = 10
dx = 10e-9
mode = 'p'

def fun(wa):
    y,_ = mul_fun(wa,w0,Nlayer,dx,omega_integrate,kbeta_integrate,epsbkg=1.,gamma=gamma,distance=distance,T0=T0,mode=mode)
    return y

grad_fun = grad(fun)

ctrl = 0
def fun_nlopt(x,gradn):
    global ctrl
    gradn[:] = grad_fun(x)
    y = fun(x)
    
    print('Step = ',ctrl,', R = ',y)
    ctrl += 1
    return fun(x)

# set up NLOPT
ndof = No*Nlayer
init = np.ones(ndof)*0.15
lb=np.zeros(ndof,dtype=float)
ub=np.ones(ndof,dtype=float)*0.2

opt = nlopt.opt(nlopt.LD_MMA, ndof)
opt.set_lower_bounds(lb)
opt.set_upper_bounds(ub)

opt.set_ftol_rel(1e-5)
opt.set_maxeval(300)

opt.set_max_objective(fun_nlopt)
x = opt.optimize(init)
np.savetxt('x.txt',x)
