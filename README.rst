* A list of required package can be found in requirement.txt
* Installation: in the directory, "pip install -e .", add "--user" if on a server
* Try pytest test/ to see if everything is working fine
* Two examples in example folder: example_flux for just computing flux, and example_nlopt for optimization
