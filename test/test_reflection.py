import numpy as np
import multilayer

try:
    import autograd.numpy as npa
    from autograd import grad
    AG_AVAILABLE = True
except ImportError:
    AG_AVAILABLE = False

kbeta = 0.2
epsilon = 4+1j
thickness = 0.5

tol=1e-3
def test_singlelayer():
    fun = multilayer.single_layer

    mode = 'p'
    rho, tau = fun(kbeta,epsilon,thickness,mode)
    
    rho_m = 0.4620-0.1267*1j
    tau_m = 0.3854+0.6325*1j
    
    assert np.abs(rho-rho_m)<tol,'wrong rho for p-polarization'
    assert np.abs(tau-tau_m)<tol,'wrong tau for p-polarization'

    mode = 's'
    rho, tau = fun(kbeta,epsilon,thickness,mode)
    
    rho_m = -0.4788 + 0.1311*1j
    tau_m = 0.3778 + 0.6241*1j
    
    assert np.abs(rho-rho_m)<tol,'wrong rho for s-polarization'
    assert np.abs(tau-tau_m)<tol,'wrong tau for s-polarization'


def test_mul():
    fun = multilayer.mul_layer
    
    r = fun(kbeta,[epsilon,epsilon],2,thickness/2,1,'p')
    rho_m = 0.4620-0.1267*1j
    assert np.abs(r-rho_m)<tol,'wrong r for p-polarization'

    r = fun(kbeta,[epsilon,epsilon],2,thickness/2,1,'s')
    rho_m = -0.4788 + 0.1311*1j
    assert np.abs(r-rho_m)<tol,'wrong r for s-polarization'
    
# if AG_AVAILABLE:
#     multilayer.set_backend('autograd')
