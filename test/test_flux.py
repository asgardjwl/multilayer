import numpy as np
import multilayer
from utils import t_grad

try:
    import autograd.numpy as npa
    from autograd import grad
    AG_AVAILABLE = True
except ImportError:
    AG_AVAILABLE = False

N = 2
epsilon = [4+1j,2+2j]
dx = 0.2
distance = 0.1
epsbkg = 1

obj = multilayer.obj(distance,epsbkg,dx,N)
obj.get_epsilon([epsilon,epsilon])

tol=5e-4
def test_omega_k_mode():
    kbeta = np.array([0.5,2])
    k0 = 1
    
    flux = obj.flux_omega_k_mode(kbeta,k0,'p')
    assert np.abs(flux[0]-0.0126)<tol,'wrong propogating flux value for p-polarization'
    assert np.abs(flux[1]-0.0102)<tol,'wrong evanescent flux value for p-polarization'

    k0 = 2
    flux = obj.flux_omega_k_mode(kbeta,k0,'s')
    assert np.abs(flux[0]-0.0340)<tol,'wrong propogating flux value for p-polarization'
    assert np.abs(flux[1]-0.0177)<tol,'wrong evanescent flux value for s-polarization'

    kbeta_integrate = [1+1e-10,10,50]
    flux = obj.flux_omega(kbeta_integrate,k0,'p')
    assert np.abs(flux-0.2944)<tol,'wrong integrated evanescent flux value for p-polarization'

    flux = obj.flux_omega(kbeta_integrate,k0,'s')
    assert np.abs(flux-0.0884)<tol,'wrong integrated evanescent flux value for s-polarization'    

    kbeta_integrate = [0,1+1e-10,50]
    flux = obj.flux_omega(kbeta_integrate,k0,'p')
    assert np.abs(flux-0.0410)<tol,'wrong integrated propogating flux value for p-polarization'

    flux = obj.flux_omega(kbeta_integrate,k0,'s')
    assert np.abs(flux-0.0369)<tol,'wrong integrated propogating flux value for p-polarization'    
    
if AG_AVAILABLE:
    multilayer.set_backend('autograd')
    def test_epsgrad():
        N = 2
        dx = 0.2
        distance = 0.1
        epsbkg = 1
        kbeta_integrate = [1+1e-10,10,50]
        k0 = 2.
        
        epsimag1 = np.array([1j,2j])
        epsimag2 = np.array([0.1j,0.3j])
        
        def fun(x):
            obj = multilayer.obj(distance,epsbkg,dx,N)
            obj.get_epsilon([x[:N]+epsimag1,x[N:]+epsimag2])
            flux = obj.flux_omega(kbeta_integrate,k0,'s')
            return flux

        grad_fun = grad(fun)
        
        x = np.random.random(N*2)
        dx = 1e-3
        ind = 2
        FD, AD = t_grad(fun,grad_fun,x,dx,ind)
        assert abs(FD-AD)<abs(FD)*tol,'wrong gradient'        
