import numpy as npa
import multilayer
from utils import t_grad

try:
    import autograd.numpy as np
    from autograd import grad
    AG_AVAILABLE = True
except ImportError:
    import numpy as np
    AG_AVAILABLE = False

def Drude(w,epinf,wp,gamma):
    return epinf - wp**2/w/(w+1j*gamma)

N = 0
epsilon = [4+1j,2+2j]
dx = 10e-9
distance = 10e-9
epsbkg = lambda w:Drude(w,1,2.5e14,1e14)
obj = multilayer.obj(distance,epsbkg,dx,N)
obj.get_epsilon(epsilon)
tol=5e-4

#def test_htc():
kbeta_integrate = [0,2000,100]
omega_integrate = [0.1e14,5e14,100]
T0 = 300
mode = 'p'
y = obj.htc_mode(T0,omega_integrate,kbeta_integrate,mode)
print(y)
