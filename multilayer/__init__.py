from .backend import backend, set_backend
from .reflectivity import single_layer, mul_layer
from .heatflux import obj
from .mpi_nlopt import nlopt_opt,bayes_opt,fun_mpi

__author__ = """Weiliang Jin"""
__email__ = 'jwlaaa@gmail.com'
__version__ = '0.1'
