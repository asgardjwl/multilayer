from mpi4py import MPI
import autograd.numpy as np
from autograd import grad
import numpy as npf
import time

try:
    import bayesopt as bo
    BO_AVAILABLE = True
except ImportError:
    BO_AVAILABLE = False

try:
    import nlopt
    NL_AVAILABLE = True
except ImportError:
    NL_AVAILABLE = False

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

class nlopt_opt:
    def __init__(self,ndof,lb,ub,maxeval,ftol,filename,savefile_N,timing=1):
        '''
        savefile_N: output dof as file every such number, with filename
        '''
        if type(lb) == float or type(lb) == int:
            lbn = lb*np.ones(ndof,dtype=float)
        else:
            lbn = lb
        if type(ub) == float or type(ub) == int:
            ubn = ub*np.ones(ndof,dtype=float)
        else:
            ubn = ub

        opt = nlopt.opt(nlopt.LD_MMA, ndof)
        opt.set_lower_bounds(lbn)
        opt.set_upper_bounds(ubn)
        opt.set_maxeval(maxeval)
        opt.set_ftol_rel(ftol)
        self.opt = opt

        self.filename = filename
        self.savefile_N = savefile_N
        self.ndof = ndof
        self.ctrl = 0
        self.timing = timing
        self.lb = lbn
        self.ub = ubn
        
    def fun_opt(self,fun,init_type,inverse=0):
        '''
        '''
        init = []
        if rank == 0:
            init = get_init(init_type,self.ndof,self.lb,self.ub)
        init = comm.bcast(init)

        def fun_nlopt(dof,gradn):
            t1 = time.time()
            val0,gn = fun_mpi(dof,fun[0],fun[1])
            if inverse == 1:
                val = 1./val0
                gn = -gn*val**2
            else:
                val = val0
            gradn[:] = gn
            t2 = time.time()

            if 'autograd' not in str(type(val)) and rank == 0:
                if self.timing == 1:
                    print(self.ctrl,'val = ',val0,'time=',t2-t1)
                else:
                    print(self.ctrl,'val = ',val0)
            
                if self.savefile_N>0 and npf.mod(self.ctrl,self.savefile_N) == 0:
                    npf.savetxt(self.filename+'dof'+str(self.ctrl)+'.txt', dof)
            self.ctrl += 1
            return val

        if inverse == 0:
            self.opt.set_max_objective(fun_nlopt)
        else:
            self.opt.set_min_objective(fun_nlopt)

        x = self.opt.optimize(init)
        return x

class bayes_opt:
    def __init__(self,ndof,lb,ub,maxeval,filename,savefile_N,timing=1,relearn=5,sample=20,inner=10):
        '''
        savefile_N: output dof as file every such number, with filename
        '''
        if type(lb) == float or type(lb) == int:
            lbn = lb*np.ones(ndof,dtype=float)
        else:
            lbn = lb
        if type(ub) == float or type(ub) == int:
            ubn = ub*np.ones(ndof,dtype=float)
        else:
            ubn = ub

        params = {} 
        params['n_iterations'] = maxeval
        params['n_iter_relearn'] =relearn
        params['n_inner_iterations']=inner
        params['n_init_samples'] = sample
        params['noise'] = 1e-10
        params['epsilon']=0 #greedy

        self.params = params
        self.filename = filename
        self.savefile_N = savefile_N
        self.ndof = ndof
        self.ctrl = 0
        self.timing = timing
        self.lb = lbn
        self.ub = ubn
        
    def fun_opt(self,fun,init_type,inverse=0):
        '''
        '''
        init = []
        if rank == 0:
            init = get_init(init_type,self.ndof,self.lb,self.ub)
        init = comm.bcast(init)

        def fun_nlopt(dof):
            t1 = time.time()
            val0,gn = fun_mpi(dof,fun[0],fun[1],isgrad=False)
            if inverse == 1:
                val = 1./val0
            else:
                val = -val0
            t2 = time.time()

            if 'autograd' not in str(type(val)) and rank == 0:
                if self.timing == 1:
                    print(self.ctrl,'val = ',val0,'time=',t2-t1)
                else:
                    print(self.ctrl,'val = ',val0)
            
                if self.savefile_N>0 and npf.mod(self.ctrl,self.savefile_N) == 0:
                    npf.savetxt(self.filename+'dof'+str(self.ctrl)+'.txt', dof)
            self.ctrl += 1
            return val

        y_out, x_out, error = bo.optimize(fun_nlopt,self.ndof,self.lb,self.ub,self.params)
        return error
    
def fun_mpi(dof,fun,N,isgrad=True):
    '''mpi parallization for fun(dof,ctrl), ctrl is the numbering of ctrl's frequency calculation
    N calculations in total
    returns the sum: sum_{ctrl=1 toN} fun(dof,ctrl)
    '''
    dof = comm.bcast(dof)

    Nloop = int(np.ceil(1.0*N/size)) # number of calculations for each node
    val_i=[]
    g_i=[]
    val=[]
    g=[]

    for i in range(0,Nloop):
        ctrl = i*size+rank
        if ctrl < N:

            funi = lambda dof: fun(dof,ctrl)
            val = funi(dof)
            val_i.append([ctrl,val])
            if isgrad == True:
                grad_fun = grad(funi)
                gval = grad_fun(dof)
                g_i.append([ctrl,gval])

    # gather the solution
    val_i = comm.gather(val_i)
    g_i = comm.gather(g_i)
    # summation
    if rank == 0:
        val_i = [x for x in val_i if x]
        val_i = npf.concatenate(npf.array(val_i))
        val = np.sum(val_i[:,1])
        
        if isgrad == True:
            g_i = [x for x in g_i if x]
            g_i = npf.concatenate(npf.array(g_i))
            g = np.sum(g_i[:,1])

    val = comm.bcast(val)
    g = comm.bcast(g)
    return val,g

def get_init(init_type,ndof,lb,ub):
    if init_type == 'rand':
        init = np.random.random(ndof)
        init = lb + (ub-lb)*init
    elif init_type == 'vac':
        init = np.zeros(ndof)+1e-5*np.random.random(ndof)
        init = lb + (ub-lb)*init
    elif init_type == 'one':
        init = np.ones(ndof)
        init = lb + (ub-lb)*init
    else:
        tmp = open(init_type,'r')
        init = np.loadtxt(tmp)
        if ndof == 1:
            init = np.array([init])
    init = init[:ndof]
    return init
