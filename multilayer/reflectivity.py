from . import backend as bd

def mul_layer(kbeta,epsilon,N,dx,epsbkg,mode,transmission=False):
    '''
    The reflectivity of N combined layers, each layer of thickness dx, dx already multiplied by k_0;
    the end of all layers, away from the interface for reflectivity computation, is semi-infinite with epsbkg;
    kbeta is normalized by k_0;
    mode = 's','p'
    '''

    if bd.isinstance(dx,float) or bd.isinstance(dx,int):
        dxl = dx*bd.ones(N)
    else:
        dxl = dx

    # starting from epsbkg
    kz = bd.sqrt(1+0j-kbeta**2)
    kzm = bd.sqrt(epsbkg +0j - kbeta**2)
    if mode == 'p':
        r = (epsbkg*kz-kzm)/(epsbkg*kz+kzm)
        t = 2*bd.sqrt(epsbkg)*kz/(epsbkg*kz+kzm)
    elif mode == 's':
        r = (kz-kzm)/(kz+kzm)
        t = 2*kz/(kz+kzm)

    # cascading
    for i in range(N):
        rho, tau = single_layer(kbeta,epsilon[i],dxl[i],mode)
        if transmission == True:
            t = tau*t/(1-r*rho)
        r = rho + tau**2*r/(1-r*rho)
    if transmission == True:
        return r,t
    else:
        return r
        
def single_layer(kbeta,epsilon,thickness,mode):
    '''
    kbeta is normalized by k_0
    thickness is already multiplied by k_0
    mode = 's','p'
    '''

    kz = bd.sqrt(1+0j-kbeta**2)
    kzm = bd.sqrt(epsilon +0j - kbeta**2)
    
    if mode == 'p':
        r = (epsilon*kz-kzm)/(epsilon*kz+kzm)
    elif mode == 's':
        r = (kz-kzm)/(kz+kzm)
    
    rho = r*(1-bd.exp(2*1j*kzm*thickness))/(1-r**2*bd.exp(2*1j*kzm*thickness))
    tau = (1-r**2)*bd.exp(1j*kzm*thickness)/(1-r**2*bd.exp(2*1j*kzm*thickness))

    return rho, tau
