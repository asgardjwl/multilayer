from . import backend as bd
from .reflectivity import mul_layer
import numpy as np
import scipy.constants as cons

class obj:
    def __init__(self,distance,epsbkg,dx,N):
        '''
        distance: between two slabs
        epsbkg: away from the separation, semi-infinite with epsbkg
        N: number of layers for each slab
        dx, distance are in actual unit
        '''
        self.distance = distance
        self.epsbkg = epsbkg
        self.dx = dx
        self.N = N

    def get_epsilon(self,epsilon):
        '''
        epsilon = [ep1, ep2], for two slabs; for each , it has length N, and the 0-th corresponds to furthest away from the interface
        '''
        self.ep1 = epsilon[0]
        self.ep2 = epsilon[1]

    def htc_mode(self,T0,omega_integrate,kbeta_integrate,mode,ctrl=None):
        ws = omega_integrate[0]
        we = omega_integrate[1]
        wN = omega_integrate[2]
        dw = (we-ws)/wN
        w = np.linspace(ws+dw/2,we-dw/2,wN)

        if ctrl is None:
            htc = []
            for omega in w:
                theta = cons.hbar*omega/(bd.exp(cons.hbar*omega/cons.k/T0)-1)
                d_theta = theta**2*bd.exp(cons.hbar*omega/cons.k/T0)/cons.k/T0**2
                k0 = omega/cons.c
                f = self.flux_omega(kbeta_integrate,k0,mode)*d_theta
                htc.append(f)
            htc = bd.array(htc)
            return bd.sum(htc)*dw,htc
        else:
            omega = w[ctrl]
            theta = cons.hbar*omega/(bd.exp(cons.hbar*omega/cons.k/T0)-1)
            d_theta = theta**2*bd.exp(cons.hbar*omega/cons.k/T0)/cons.k/T0**2
            k0 = omega/cons.c
            f = self.flux_omega(kbeta_integrate,k0,mode)*d_theta
            return f*dw
        
    def flux_omega(self,kbeta_integrate,k0,mode):
        '''
        integrate over kbeta

        for now, evenly sampled integration; note, there can be singularity at kbeta = 1, so separtely integrate over propogating and evanescent waves

        kbeta_integrate = [k_start,k_end,k_number], the 1st two normalized by k0
        k0 = omega/c
        mode = 's'/'p'
        '''
        ks = kbeta_integrate[0]
        ke = kbeta_integrate[1]
        kN = kbeta_integrate[2]
        
        dk = (ke-ks)/kN
        kbeta = np.linspace(ks+dk/2,ke-dk/2,kN)

        f = self.flux_omega_k_mode(kbeta,k0,mode)
        return bd.sum(f)*dk
    
    def flux_omega_k_mode(self,kbeta,k0,mode,spectrum=False):
        '''
        k0 = omega/c
        kbeta: normalized by k0, kbeta can be an numpy array
        '''

        omega = k0*cons.c
        if callable(self.epsbkg):
            epsbkg = self.epsbkg(omega)
        else:
            epsbkg = self.epsbkg

        if callable(self.ep1):
            ep1 = self.ep1(omega)
        else:
            ep1 = self.ep1

        if callable(self.ep2):
            ep2 = self.ep2(omega)
        else:
            ep2 = self.ep2
            
        R1 = mul_layer(kbeta,ep1,self.N,self.dx*k0,epsbkg,mode)
        R2 = mul_layer(kbeta,ep2,self.N,self.dx*k0,epsbkg,mode)

        kz = bd.sqrt(1+0j-kbeta**2)*k0
        phi_e = 4*bd.imag(R1)*bd.imag(R2)*bd.exp(-2*bd.imag(kz)*self.distance)/bd.abs(1-R1*R2*bd.exp(-2*bd.imag(kz)*self.distance))**2
        phi_p = (1-bd.abs(R1)**2)*(1-bd.abs(R2)**2)/bd.abs(1-R1*R2*bd.exp(2*1j*bd.real(kz)*self.distance))**2
        phi = bd.where(kbeta<1,phi_p,phi_e)
        
        if spectrum == False:
            phi = phi * kbeta * k0**2/(2*bd.pi)**2

        return phi
                          


